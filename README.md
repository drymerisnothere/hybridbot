# hybridbot

Hybridbot is a bot that relays messages from a IRC channel to an XMPP MUC and vice versa. First of all, you need to install slixmpp and irc.

	su -c "pip3 install slixmpp irc"
	# or
	su -c "pip3 install -r requirements.txt"

It's configuration is simple, you have to create a config file per relay, as this example (remember to change the values):

    [Shared]
    prefix   = .
    owner    = somebody

    [IRC]
    channel  = #daemons
    nick     = pasarela
    server   = chat.freenode.net
    port     = 6667

    [XMPP]
    jid      = becario@daemons.cf
    password = goodpassword
    muc      = testeando@salas.daemons.cf
    nick     = pasarela

Most of it is pretty obvious, the only two lines that need explanation are the ones in Shared.

The option "prefix" is a string that gets prefixed before the command like "!help" if prefix=!.

The option "owner" is a string that will be printed when issuing the ".help" command. It can be just something like "drymer on XMPP MUC" or just a name, your choice.

To execute it, just do:

    python3 hybridbot.py myconfig.ini

Or if the config file is named "config.ini", just do:

    python3 hybridbot.py

And on the MUC or the IRC channel, you can issue two commands, ".help" and ".users".
